﻿using System;
using System.Data.SqlClient;
using System.IO;
using Telegram.Bot;
using Telegram.Bot.Types.InputFiles;

namespace TelegramBot.Example
{
    class Bd
    {
        public int id { get; set; }
        public string name { get; set; }
        public string rating { get; set; }
        public int year { get; set; }
        public string country { get; set; }
        public string description { get; set; }
        public int duration { get; set; }
        public string genre { get; set; }
        public Bd()
        {
            id = id;
            name = name;
            rating = rating;
            year = year;
            country = country;
            description = description;
            duration = duration;
            genre = genre;
        }

    }
    class BdReview
    {
        public string name { get; set; }
        public string rating { get; set; }
        public string description { get; set; }
        public BdReview()
        {
            name = name;
            rating = rating;
            description = description;
        }

    }
    class Program
    {
        public static string str, str2, str3;
        private static readonly TelegramBotClient botClient = new TelegramBotClient("1700577313:AAH8HxKdkYelonjH4wRZmstuNFC_RMiXWC4");
        static void Main(string[] args)
        {
            botClient.OnMessage += Start; //Создаём обработчик событий на сообщение
            botClient.OnMessageEdited += Start; //Начинаем получать апдейты с сервера телеграмма

            botClient.StartReceiving();
            Console.ReadLine();
            botClient.StartReceiving();
        }
        private static void Start(object sender, Telegram.Bot.Args.MessageEventArgs e) // для первого сообщения
        {
            if (e.Message.Type == Telegram.Bot.Types.Enums.MessageType.Text)
            {
                botClient.OnMessage -= Start;
                botClient.OnMessageEdited -= Start;
                botClient.SendTextMessageAsync(e.Message.Chat.Id, "Привет! Тебе нужна помощь? Пиши \"Да\", если нужен совет.");
                botClient.OnMessage += BotClient_OnMessage2; // для второго сообщения
                botClient.OnMessageEdited += BotClient_OnMessage2; // для второго сообщения
            }
        }
        private static void BotClient_OnMessage2(object sender, Telegram.Bot.Args.MessageEventArgs e) // для меню
        {
            if (e.Message.Type == Telegram.Bot.Types.Enums.MessageType.Text)
            {
                if (e.Message.Text == "Да" || e.Message.Text == "да")
                {
                    botClient.OnMessage -= BotClient_OnMessage2;
                    botClient.OnMessageEdited -= BotClient_OnMessage2;
                    botClient.SendTextMessageAsync(e.Message.Chat.Id, "Пожалуйста, отправь мне сообщение с цифрой, соответствующей выбранному тобой пунктом.\n1. Рандомный фильм\n2. 7 фильмов на каждый день\n3. Поиск фильма\n4. Отзывы");

                    botClient.OnMessage += BotClient_OnMessage3; // для третьего сообщения
                    botClient.OnMessageEdited += BotClient_OnMessage3; // для третьего сообщения
                }
                else
                {
                    botClient.SendTextMessageAsync(e.Message.Chat.Id, "Я тебя не понимаю. Повтори свой ответ");
                }
            }
        }
        private static void BotClient_OnMessage3(object sender, Telegram.Bot.Args.MessageEventArgs e) // для третьего сообщения
        {
            botClient.OnMessage -= BotClient_OnMessage3; // тогда можно много раз выбирать меню
            botClient.OnMessageEdited -= BotClient_OnMessage3;

            int n = 100;
            Bd[] bd = new Bd[n];
            for (int i = 0; i < n; i++)
            {
                bd[i] = new Bd();
            }
            using (var connection = new SqlConnection("Data Source=DESKTOP-U99NK91\\SQLEPRESS; Initial Catalog = БотФильмы; User ID = sa; Password = 123456")) // чтение данных из бд
            {
                connection.Open();
                using (var cmd = new SqlCommand("SELECT ID FROM [БотФильмы].[dbo].[ФильмыСериалы]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (rd.Read())
                            {
                                bd[i].id = Convert.ToInt32(rd.GetValue(0));
                            }
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Название FROM [БотФильмы].[dbo].[ФильмыСериалы]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (rd.Read())
                            {
                                bd[i].name = rd.GetValue(0).ToString();
                            }
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Оценка FROM [БотФильмы].[dbo].[ФильмыСериалы]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (rd.Read())
                            {
                                bd[i].rating = rd.GetValue(0).ToString();
                            }
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT [Год выпуска] FROM [БотФильмы].[dbo].[ФильмыСериалы]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (rd.Read())
                            {
                                bd[i].year = Convert.ToInt32(rd.GetValue(0));
                            }
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Страна FROM [БотФильмы].[dbo].[ФильмыСериалы]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (rd.Read())
                            {
                                bd[i].country = rd.GetValue(0).ToString();
                            }
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Описание FROM [БотФильмы].[dbo].[ФильмыСериалы]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (rd.Read())
                            {
                                bd[i].description = rd.GetValue(0).ToString();
                            }
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Продолжительность FROM [БотФильмы].[dbo].[ФильмыСериалы]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (rd.Read())
                            {
                                bd[i].duration = Convert.ToInt32(rd.GetValue(0));
                            }
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Жанр FROM [БотФильмы].[dbo].[ФильмыСериалы]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (rd.Read())
                            {
                                bd[i].genre = rd.GetValue(0).ToString();
                            }
                        }
                    }
                }
                connection.Close();
            }

            if (e.Message.Type == Telegram.Bot.Types.Enums.MessageType.Text)
            {
                int num;
                bool isInt = int.TryParse(e.Message.Text, out num);
                //num = Convert.ToInt32(e.Message.Text);
                if (isInt)
                {
                    switch (num)
                    {
                        case 1: // рандомный фильм
                            int a = 0;
                            Random r = new Random();
                            a = r.Next(1, 100);
                            InputOnlineFile imageFile = new InputOnlineFile(new MemoryStream(System.IO.File.ReadAllBytes("C:\\Users\\37533\\Desktop\\БОТ\\для фильмов фото\\" + a + ".JPG")));
                            botClient.SendPhotoAsync(e.Message.Chat.Id, imageFile);
                            a--;
                            botClient.SendTextMessageAsync(e.Message.Chat.Id, bd[a].name + " [" + bd[a].year + ", " + bd[a].genre + ", " + bd[a].country + "]\n\n" +
                                bd[a].duration + " мин.\n" + bd[a].description + "\n\nРейтинг Киного - " + bd[a].rating + "\n");

                            botClient.SendTextMessageAsync(e.Message.Chat.Id, "Пожалуйста, отправь мне сообщение с цифрой, соответствующей выбранному тобой пунктом.\n1. Рандомный фильм\n2. 7 фильмов на каждый день\n3. Поиск фильма\n4. Отзывы");
                            botClient.OnMessage += BotClient_OnMessage3;
                            botClient.OnMessageEdited += BotClient_OnMessage3;
                            break;
                        case 2: // 7 фильмов
                            string str = "ПОНЕДЕЛЬНИК\n" + "«Завтрак на Плутоне», 2005.Режиссер: Нил Джордан\n" + "Этот фильм стоит смотреть только ради Киллиана Мёрфи. Вернее так - только ради Киллиана Мёрфи стоит посмотреть этот фильм. История о мальчике, от которого отказалась мама, его жизнь почти на всех поворотах напоминает ад - ирландские сепаратисты, жестокая мачеха, непонимание и несоответствие среды тому, что он чувствует внутри. Но остается верен мечте и хранит свет в своем сердце. Режиссер обыгрывает ЛГБТ-проблемы так легко, что обвинить его в какой-либо пропаганде просто нереально. А Мёрфи играет на радость зрителю - это уморительно, а списанный частично с «Шоу ужасов Рики Хоррора» герой даже умиляет вне зависимости от сексуальных предпочтений зрителя."
                            + "\n\nВТОРНИК\n" + "«Логан», 2017. Режиссер: Джеймс Мэнголд+n" + "Исключительно для тех, кто любит «Людей Икс», комиксы, Росомаху и Хью Джекмана, и тех, кто очень хочет увидеть еще хотя бы один фильм с ним. При просмотре постигло разочарование. Потому как модная тенденция делать из фильмов по комиксам серьезные драмы сейчас зашла на мой взгляд в тупик. Уже не понятно, зачем все эти когти, телекинез, если зрелище все больше напоминает банальный боевик. А в «Логане» еще и прием выбран абсолютно заурядный - по-«Леон»овски - старый солдат, не знающий слов любви, и маленькая девочка..."
                            + "\n\nСРЕДА\n" + "«Филомена», 2013.Режиссер: Стивен Фрирз\n" + "Трогательный, в хорошем смысле, фильм о пожилой ирландке, только послушайте - посмотрите в оригинале, как говорит Джуди Денч.Это просто песня - «бьюжифул», словно британку укусил вайнах и кто-то из Вологды. Темы в «Филомене» подняты актуальные -бесчинства церкви, ранняя беременность, этика журналиста, материнская любовь, гомосексуализм, СПИД. Смешали все, но получилось по-британски элегантно.А для меня лично это еще и настоящая история, которую должны рассказывать журналисты, не размениваясь на то, что сейчас у нас называется сенсация. Кстати, кино основано на реальных событиях."
                            + "\n\nЧЕТВЕРГ\n" + "«Язык бабочек», 1999. Режиссер: Хосе Луис Куэрда\n" + "Испанский фильм о том, как мир взрослых врывается в мир детства и разрушает его, оставляя после себя растерянные слезы ребенка, осознавшего, что той чистоты больше уже никогда не будет. История маленького Мончо, пошедшего в первый класс, его страхи и открытия, сделанные в стенах школы, происходят тогда, когда за окном устанавливается диктатура Франко. Людей клеймят предателями только за лозунги, а дети становятся разменной монетой властей и испуганных родителей. Заставить ребенка кричать: «Предатель!» - самое большое преступление, а не убийства, госперевороты. Мончо еще не знает, кто это - «предатель», но в его жизни появляется этот ярлык навечно."
                            + "\n\nПЯТНИЦА\n" + "«Стажер», 2015. Режиссер: Нэнси Майерс\n" + "Если бы не совет друга - я бы триста раз прошел мимо этого фильма. Но Нэнси Майерс умеет завлечь с самого начала своей искренностью. Почему я обожаю пересматривать ее «Отпуск по обмену». А тут еще и Роберт де Ниро, который поразительно умеет перевоплощаться из мафиози в трогательного пенсионера, а Энн Хэтэуэй словно снимается во второй серии «Дьявол носит Прада», став наконец-то начальницей. Это кино для хорошего настроения, чтобы отвлечься и вспомнить о том, что важнее карьера или семья. Вывод Мэйерс делает неожиданный.";
                            string str2 = "\n\nСУББОТА\n" + "«Моя девушка - монстр», 2016. Режиссер: Начо Вигаландо\n" + "Годзилла, алкоголичка, парень-манипулятор - что общего? Посмотрите этот фильм. Сюжет закручен очень лихо и безумно. Не знаю, как писали сценарий, но думаю не без каких-то веществ. Хэтэуэй, да снова она, прекрасно играет вечно пьяную девчуху, которая вырубается где попало. А вот ее партнер по фильму Джейсон Судейкис, еще раз убедился, меня просто раздражает. Человек с настолько не пластичным лицом, ничего не выражающим, что ему позавидует даже Кристен Стюарт. До этого помню, что он меня бесил в фильме «Несносные леди». Здесь ситуация только обострилась."
                            + "\n\nВОСКРЕСЕНЬЕ\n" + "«Дикая история», 2016. Режиссер: Алекс де ла Иглесиа\n" + "Испанский замес на тему - «Обители зла», европейских страхов и стереотипов - теракты, арабы, хипстеры и так далее. Смотрится легко, местами противно. Но в большой компании под пиццу с поп-корном однозначно пойдет. И для тех, кому нравятся психологические хорроры. Не скажу что очень страшно, но есть ритм, интересные задумки и заговоры-заговоры-паранойя.";
                            botClient.SendTextMessageAsync(e.Message.Chat.Id, str);
                            botClient.SendTextMessageAsync(e.Message.Chat.Id, str2);
                            botClient.OnMessage += BotClient_OnMessage3;
                            botClient.OnMessageEdited += BotClient_OnMessage3;
                            botClient.SendTextMessageAsync(e.Message.Chat.Id, "Пожалуйста, отправь мне сообщение с цифрой, соответствующей выбранному тобой пунктом.\n1. Рандомный фильм\n2. 7 фильмов на каждый день\n3. Поиск фильма\n4. Отзывы");
                            break;
                        case 3: // поиск фильма
                            botClient.SendTextMessageAsync(e.Message.Chat.Id, "Пожалуйста, отправь мне сообщение с цифрой, соответствующей выбранному тобой пунктом.\n1. Поиск фильма по названию\n2. Поиск фильма по жанру");
                            botClient.OnMessage += BotClient_OnMessage4;
                            botClient.OnMessageEdited += BotClient_OnMessage4;
                            break;
                        case 4: // отзывы
                            botClient.SendTextMessageAsync(e.Message.Chat.Id, "Пожалуйста, отправь мне сообщение с цифрой, соответствующей выбранному тобой пунктом.\n1. Просмотр отзывов на выбранный вами фильм\n2. Написание отзыва на выбранный вами фильм");
                            botClient.OnMessage += BotClient_OnMessage6;
                            botClient.OnMessageEdited += BotClient_OnMessage6;
                            break;
                        default:
                            botClient.SendTextMessageAsync(e.Message.Chat.Id, "Упс! Что-то пошло не так! Повтори свой ввод");
                            botClient.OnMessage += BotClient_OnMessage3;
                            botClient.OnMessageEdited += BotClient_OnMessage3;
                            break;
                    }
                }
                else
                {
                    botClient.SendTextMessageAsync(e.Message.Chat.Id, "Упс! Что-то пошло не так! Повтори свой ввод");
                    botClient.OnMessage += BotClient_OnMessage3;
                    botClient.OnMessageEdited += BotClient_OnMessage3;
                }
            }
        }
        private static void BotClient_OnMessage4(object sender, Telegram.Bot.Args.MessageEventArgs e) // для выбора критерия поиска
        {
            botClient.OnMessage -= BotClient_OnMessage4;
            botClient.OnMessageEdited -= BotClient_OnMessage4;

            if (e.Message.Type == Telegram.Bot.Types.Enums.MessageType.Text)
            {
                if (e.Message.Text == "1") 
                {
                    botClient.SendTextMessageAsync(e.Message.Chat.Id, "Введите название фильма");
                    botClient.OnMessage += BotClient_OnMessage5;
                    botClient.OnMessageEdited += BotClient_OnMessage5;
                }
                else if (e.Message.Text == "2")
                {
                    botClient.SendTextMessageAsync(e.Message.Chat.Id, "Пожалуйста, отправь мне сообщение с цифрой нужного тебе жанра.\n1. Боевик\n2. Комедия\n3. Триллер\n4. Мелодрама\n5. Ужасы\n6. Не имеет значения");
                    botClient.OnMessage += BotClient_OnMessage5;
                    botClient.OnMessageEdited += BotClient_OnMessage5;
                }
                else
                {
                    botClient.SendTextMessageAsync(e.Message.Chat.Id, "Упс! Что-то пошло не так! Повтори свой ввод");
                    botClient.OnMessage += BotClient_OnMessage4;
                    botClient.OnMessageEdited += BotClient_OnMessage4;
                }
            }
        }
        private static void BotClient_OnMessage5(object sender, Telegram.Bot.Args.MessageEventArgs e) // для поиска названия или жанра
        {
            botClient.OnMessage -= BotClient_OnMessage5;
            botClient.OnMessageEdited -= BotClient_OnMessage5;

            int n = 100, a = 0;
            Bd[] bd = new Bd[n];
            for (int i = 0; i < n; i++)
            {
                bd[i] = new Bd();
            }
            using (var connection = new SqlConnection("Data Source=DESKTOP-U99NK91\\SQLEPRESS; Initial Catalog = БотФильмы; User ID = sa; Password = 123456")) // чтение данных из бд
            {
                connection.Open();
                using (var cmd = new SqlCommand("SELECT ID FROM [БотФильмы].[dbo].[ФильмыСериалы]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (rd.Read())
                            {
                                bd[i].id = Convert.ToInt32(rd.GetValue(0));
                            }
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Название FROM [БотФильмы].[dbo].[ФильмыСериалы]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (rd.Read())
                            {
                                bd[i].name = rd.GetValue(0).ToString();
                            }
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Оценка FROM [БотФильмы].[dbo].[ФильмыСериалы]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (rd.Read())
                            {
                                bd[i].rating = rd.GetValue(0).ToString();
                            }
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT [Год выпуска] FROM [БотФильмы].[dbo].[ФильмыСериалы]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (rd.Read())
                            {
                                bd[i].year = Convert.ToInt32(rd.GetValue(0));
                            }
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Страна FROM [БотФильмы].[dbo].[ФильмыСериалы]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (rd.Read())
                            {
                                bd[i].country = rd.GetValue(0).ToString();
                            }
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Описание FROM [БотФильмы].[dbo].[ФильмыСериалы]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (rd.Read())
                            {
                                bd[i].description = rd.GetValue(0).ToString();
                            }
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Продолжительность FROM [БотФильмы].[dbo].[ФильмыСериалы]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (rd.Read())
                            {
                                bd[i].duration = Convert.ToInt32(rd.GetValue(0));
                            }
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Жанр FROM [БотФильмы].[dbo].[ФильмыСериалы]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (rd.Read())
                            {
                                bd[i].genre = rd.GetValue(0).ToString();
                            }
                        }
                    }
                }
                connection.Close();
            }

            if (e.Message.Type == Telegram.Bot.Types.Enums.MessageType.Text)
            {
                if (e.Message.Text == "1" || e.Message.Text == "2" || e.Message.Text == "3" || e.Message.Text == "4" || e.Message.Text == "5" || e.Message.Text == "6")
                {
                    botClient.SendTextMessageAsync(e.Message.Chat.Id, "Сейчас подберем для вас что-нибудь интересненькое!");
                    if (e.Message.Text == "1")
                    {
                        do
                        {
                            Random r = new Random();
                            a = r.Next(1, 100);
                            a--;
                        }
                        while (bd[a].genre != "Боевик");
                        a++;
                        InputOnlineFile imageFile = new InputOnlineFile(new MemoryStream(System.IO.File.ReadAllBytes("C:\\Users\\37533\\Desktop\\БОТ\\для фильмов фото\\" + a + ".JPG")));
                        botClient.SendPhotoAsync(e.Message.Chat.Id, imageFile);
                        a--;
                        botClient.SendTextMessageAsync(e.Message.Chat.Id, bd[a].name + " [" + bd[a].year + ", " + bd[a].genre + ", " + bd[a].country + "]\n\n" +
                            bd[a].duration + " мин.\n" + bd[a].description + "\n\nРейтинг Киного - " + bd[a].rating + "\n");
                    }
                    else if (e.Message.Text == "2")
                    {
                        do
                        {
                            Random r = new Random();
                            a = r.Next(1, 100);
                            a--;
                        }
                        while (bd[a].genre != "Комедия");
                        a++;
                        InputOnlineFile imageFile = new InputOnlineFile(new MemoryStream(System.IO.File.ReadAllBytes("C:\\Users\\37533\\Desktop\\БОТ\\для фильмов фото\\" + a + ".JPG")));
                        botClient.SendPhotoAsync(e.Message.Chat.Id, imageFile);
                        a--;
                        botClient.SendTextMessageAsync(e.Message.Chat.Id, bd[a].name + " [" + bd[a].year + ", " + bd[a].genre + ", " + bd[a].country + "]\n\n" +
                            bd[a].duration + " мин.\n" + bd[a].description + "\n\nРейтинг Киного - " + bd[a].rating + "\n");
                    }
                    else if (e.Message.Text == "3")
                    {
                        do
                        {
                            Random r = new Random();
                            a = r.Next(1, 100);
                            a--;
                        }
                        while (bd[a].genre != "Триллер");
                        a++;
                        InputOnlineFile imageFile = new InputOnlineFile(new MemoryStream(System.IO.File.ReadAllBytes("C:\\Users\\37533\\Desktop\\БОТ\\для фильмов фото\\" + a + ".JPG")));
                        botClient.SendPhotoAsync(e.Message.Chat.Id, imageFile);
                        a--;
                        botClient.SendTextMessageAsync(e.Message.Chat.Id, bd[a].name + " [" + bd[a].year + ", " + bd[a].genre + ", " + bd[a].country + "]\n\n" +
                            bd[a].duration + " мин.\n" + bd[a].description + "\n\nРейтинг Киного - " + bd[a].rating + "\n");
                    }
                    else if (e.Message.Text == "4")
                    {
                        do
                        {
                            Random r = new Random();
                            a = r.Next(1, 100);
                            a--;
                        }
                        while (bd[a].genre != "Мелодрама");
                        a++;
                        InputOnlineFile imageFile = new InputOnlineFile(new MemoryStream(System.IO.File.ReadAllBytes("C:\\Users\\37533\\Desktop\\БОТ\\для фильмов фото\\" + a + ".JPG")));
                        botClient.SendPhotoAsync(e.Message.Chat.Id, imageFile);
                        a--;
                        botClient.SendTextMessageAsync(e.Message.Chat.Id, bd[a].name + " [" + bd[a].year + ", " + bd[a].genre + ", " + bd[a].country + "]\n\n" +
                            bd[a].duration + " мин.\n" + bd[a].description + "\n\nРейтинг Киного - " + bd[a].rating + "\n");
                    }
                    else if (e.Message.Text == "5")
                    {
                        do
                        {
                            Random r = new Random();
                            a = r.Next(1, 100);
                            a--;
                        }
                        while (bd[a].genre != "Ужасы");
                        a++;
                        InputOnlineFile imageFile = new InputOnlineFile(new MemoryStream(System.IO.File.ReadAllBytes("C:\\Users\\37533\\Desktop\\БОТ\\для фильмов фото\\" + a + ".JPG")));
                        botClient.SendPhotoAsync(e.Message.Chat.Id, imageFile);
                        a--;
                        botClient.SendTextMessageAsync(e.Message.Chat.Id, bd[a].name + " [" + bd[a].year + ", " + bd[a].genre + ", " + bd[a].country + "]\n\n" +
                            bd[a].duration + " мин.\n" + bd[a].description + "\n\nРейтинг Киного - " + bd[a].rating + "\n");
                    }
                    else if (e.Message.Text == "6")
                    {
                        Random r = new Random();
                        a = r.Next(1, 100);
                        a--;
                        a++;
                        InputOnlineFile imageFile = new InputOnlineFile(new MemoryStream(System.IO.File.ReadAllBytes("C:\\Users\\37533\\Desktop\\БОТ\\для фильмов фото\\" + a + ".JPG")));
                        botClient.SendPhotoAsync(e.Message.Chat.Id, imageFile);
                        a--;
                        botClient.SendTextMessageAsync(e.Message.Chat.Id, bd[a].name + " [" + bd[a].year + ", " + bd[a].genre + ", " + bd[a].country + "]\n\n" +
                            bd[a].duration + " мин.\n" + bd[a].description + "\n\nРейтинг Киного - " + bd[a].rating + ",00\n");
                    }
                }
                else
                {
                    int kol = 0;
                    for (int i = 0; i < bd.Length; i++)
                    {
                        if (e.Message.Text == bd[i].name)
                        {
                            InputOnlineFile imageFile = new InputOnlineFile(new MemoryStream(System.IO.File.ReadAllBytes("C:\\Users\\37533\\Desktop\\БОТ\\для фильмов фото\\" + ++i + ".JPG")));
                            i--;
                            botClient.SendPhotoAsync(e.Message.Chat.Id, imageFile);
                            botClient.SendTextMessageAsync(e.Message.Chat.Id, bd[i].name + " [" + bd[i].year + ", " + bd[i].genre + ", " + bd[i].country + "]\n\n" +
                        bd[i].duration + " мин.\n" + bd[i].description + "\n\nРейтинг Киного - " + bd[i].rating + "\n");
                            break;
                        }
                        else
                        {
                            kol++;
                        }
                    }

                    if (kol == n)
                    {
                        botClient.SendTextMessageAsync(e.Message.Chat.Id, "Прости, такого фильма мы не нашли");
                    }
                }
            }

            botClient.OnMessage += BotClient_OnMessage3;
            botClient.OnMessageEdited += BotClient_OnMessage3;
            botClient.SendTextMessageAsync(e.Message.Chat.Id, "Пожалуйста, отправь мне сообщение с цифрой, соответствующей выбранному тобой пунктом.\n1. Рандомный фильм\n2. 7 фильмов на каждый день\n3. Поиск фильма\n4. Отзывы");
        }
        private static void BotClient_OnMessage6(object sender, Telegram.Bot.Args.MessageEventArgs e) // для выбора просмотра или написания отзыва
        {
            botClient.OnMessage -= BotClient_OnMessage6;
            botClient.OnMessageEdited -= BotClient_OnMessage6;

            if (e.Message.Type == Telegram.Bot.Types.Enums.MessageType.Text)
            {
                if (e.Message.Text == "1") // вывод отзывов
                {
                    botClient.SendTextMessageAsync(e.Message.Chat.Id, "Введите название фильма");
                    botClient.OnMessage += BotClient_OnMessage7;
                    botClient.OnMessageEdited += BotClient_OnMessage7;
                }
                else if (e.Message.Text == "2") // написать отзыв
                {
                    botClient.SendTextMessageAsync(e.Message.Chat.Id, "Введите название фильма");
                    botClient.OnMessage += BotClient_OnMessage8;
                    botClient.OnMessageEdited += BotClient_OnMessage8;
                }
                else
                {
                    botClient.OnMessage += BotClient_OnMessage6;
                    botClient.OnMessageEdited += BotClient_OnMessage6;
                    botClient.SendTextMessageAsync(e.Message.Chat.Id, "Упс! Что-то пошло не так! Повтори свой ввод");
                }
            }
        }
        private static void BotClient_OnMessage8(object sender, Telegram.Bot.Args.MessageEventArgs e) // для написания отзыва
        {
            botClient.OnMessage -= BotClient_OnMessage8;
            botClient.OnMessageEdited -= BotClient_OnMessage8;

            if (e.Message.Type == Telegram.Bot.Types.Enums.MessageType.Text)
            {
                str = (e.Message.Text); // название фильма
                botClient.SendTextMessageAsync(e.Message.Chat.Id, "Напишите свой отзыв на фильм в одном сообщении");
                botClient.OnMessage += BotClient_OnMessage9;
                botClient.OnMessageEdited += BotClient_OnMessage9;
            }
        }
        private static void BotClient_OnMessage9(object sender, Telegram.Bot.Args.MessageEventArgs e) // для написания отзыва 2
        {
            botClient.OnMessage -= BotClient_OnMessage9;
            botClient.OnMessageEdited -= BotClient_OnMessage9;

            if (e.Message.Type == Telegram.Bot.Types.Enums.MessageType.Text)
            {
                str2 = (e.Message.Text); // отзыв
                botClient.SendTextMessageAsync(e.Message.Chat.Id, "Напишите оценку, которую вы бы поставили фильму от 1 до 5");
                botClient.OnMessage += BotClient_OnMessage10;
                botClient.OnMessageEdited += BotClient_OnMessage10;
            }
        }
        private static void BotClient_OnMessage10(object sender, Telegram.Bot.Args.MessageEventArgs e) // для написания отзыва 3
        {
            botClient.OnMessage -= BotClient_OnMessage10;
            botClient.OnMessageEdited -= BotClient_OnMessage10;

            if (e.Message.Type == Telegram.Bot.Types.Enums.MessageType.Text)
            {
                str3 = (e.Message.Text); // оценка фильма
                using (var connection = new SqlConnection("Data Source=DESKTOP-U99NK91\\SQLEPRESS; Initial Catalog = БотФильмы; User ID = sa; Password = 123456")) // запись данных в бд
                {
                    connection.Open();
                    string sql = string.Format("INSERT INTO Отзывы (ИмяФильма, Описание, Рейтинг)" + "VALUES ('{0}', '{1}', '{2}')", str, str2, str3);
                    using (var cmd = new SqlCommand(sql, connection))
                    {
                        cmd.ExecuteNonQuery();
                    }
                    connection.Close();
                }

                botClient.SendTextMessageAsync(e.Message.Chat.Id, "Отзыв добавлен!");
                botClient.SendTextMessageAsync(e.Message.Chat.Id, "Пожалуйста, отправь мне сообщение с цифрой, соответствующей выбранному тобой пунктом.\n1. Рандомный фильм\n2. 7 фильмов на каждый день\n3. Поиск фильма\n4. Отзывы");
                botClient.OnMessage += BotClient_OnMessage3;
                botClient.OnMessageEdited += BotClient_OnMessage3;
            }
        }
        private static void BotClient_OnMessage7(object sender, Telegram.Bot.Args.MessageEventArgs e) // для вывода отзыва
        {
            botClient.OnMessage -= BotClient_OnMessage7;
            botClient.OnMessageEdited -= BotClient_OnMessage7;

            int n = 25, k = 0;
            BdReview[] bd2 = new BdReview[n];
            for (int i = 0; i < n; i++)
            {
                bd2[i] = new BdReview();
            }

            using (var connection = new SqlConnection("Data Source=DESKTOP-U99NK91\\SQLEPRESS; Initial Catalog = БотФильмы; User ID = sa; Password = 123456")) // чтение данных из бд
            {
                connection.Open();
                using (var cmd = new SqlCommand("SELECT ИмяФильма FROM [БотФильмы].[dbo].[Отзывы]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (rd.Read())
                            {
                                bd2[i].name = rd.GetValue(0).ToString();
                            }
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Описание FROM [БотФильмы].[dbo].[Отзывы]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (rd.Read())
                            {
                                bd2[i].description = rd.GetValue(0).ToString();
                            }
                        }
                    }
                }
                using (var cmd = new SqlCommand("SELECT Рейтинг FROM [БотФильмы].[dbo].[Отзывы]", connection))
                {
                    using (var rd = cmd.ExecuteReader())
                    {
                        for (int i = 0; i < n; i++)
                        {
                            if (rd.Read())
                            {
                                bd2[i].rating = rd.GetValue(0).ToString();
                            }
                        }
                    }
                }
                connection.Close();
            }

            if (e.Message.Type == Telegram.Bot.Types.Enums.MessageType.Text)
            {
                for (int i = 0; i < bd2.Length; i++)
                {
                    if (e.Message.Text == bd2[i].name)
                    {
                        botClient.SendTextMessageAsync(e.Message.Chat.Id, "Отзыв №" + ++k + "\n" + bd2[i].description + "\n\nРейтинг - " + bd2[i].rating + "\n");
                    }
                }

                if (k == 0)
                {
                    botClient.SendTextMessageAsync(e.Message.Chat.Id, "Фильм не найден! Повтори свой ввод");
                    botClient.OnMessage += BotClient_OnMessage7;
                    botClient.OnMessageEdited += BotClient_OnMessage7;
                }
                else
                {
                    botClient.SendTextMessageAsync(e.Message.Chat.Id, "Пожалуйста, отправь мне сообщение с цифрой, соответствующей выбранному тобой пунктом.\n1. Рандомный фильм\n2. 7 фильмов на каждый день\n3. Поиск фильма\n4. Отзывы");
                    botClient.OnMessage += BotClient_OnMessage3;
                    botClient.OnMessageEdited += BotClient_OnMessage3;
                }

                k = 0;
            }
        }
    }
}