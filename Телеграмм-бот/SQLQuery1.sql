USE [БотФильмы]
GO
/****** Object:  Trigger [dbo].[ОтзывыСуществ]    Script Date: 28.05.2021 16:12:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[ОтзывыСуществ] ON [dbo].[Отзывы] 
FOR INSERT
AS
IF EXISTS(SELECT * FROM Отзывы, inserted WHERE inserted.ИмяФильма = Отзывы.ИмяФильма)
BEGIN
    ROLLBACK TRAN
END


CREATE TRIGGER ОтзывыСуществ
   ON  Отзывы
   FOR INSERT
AS 
DECLARE @ИмяФильма nvarchar(max)
SELECT @ИмяФильма = ИмяФильма FROM inserted
IF EXISTS(SELECT ИмяФильма FROM Отзывы WHERE ИмяФильма = @ИмяФильма GROUP BY ИмяФильма HAVING COUNT(ИмяФильма ) > 1)
ROLLBACK tran