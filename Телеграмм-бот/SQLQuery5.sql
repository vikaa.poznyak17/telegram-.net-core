USE [���������]
GO

/****** Object:  Table [dbo].[�������������]    Script Date: 28.05.2021 16:46:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[�������������](
	[ID] [int] NOT NULL,
	[��������] [nvarchar](50) NOT NULL,
	[������] [decimal](2, 1) NOT NULL,
	[��� �������] [int] NOT NULL,
	[������] [nvarchar](20) NOT NULL,
	[��������] [nvarchar](max) NOT NULL,
	[�����������������] [int] NOT NULL,
	[����] [nvarchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [un_name] UNIQUE NONCLUSTERED 
(
	[��������] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[�������������]  WITH CHECK ADD CHECK  (([��� �������]>(1850)))
GO

ALTER TABLE [dbo].[�������������]  WITH CHECK ADD CHECK  (([������]>=(0)))
GO


CREATE TRIGGER �������������
   ON  �������������
   FOR INSERT
AS 
DECLARE @�������� nvarchar(max)
SELECT @�������� = �������� FROM inserted
DECLARE @�������� nvarchar(max)
SELECT @�������� = �������� FROM inserted
IF EXISTS(SELECT �������� FROM ������������� WHERE �������� = @�������� GROUP BY �������� HAVING COUNT(��������) > 1) AND EXISTS(SELECT �������� FROM ������������� WHERE �������� = @�������� GROUP BY �������� HAVING COUNT(��������) > 1)
ROLLBACK tran